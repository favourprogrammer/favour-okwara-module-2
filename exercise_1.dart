void main(List<String> args) {
  // Step #1: store & print data
  String name = "Favour Okwara";
  String favoriteApp = "Notion";
  String city = "Johannesburg";

  print("Hi, I'm $name and I'm from $city.");
  print("I love using $favoriteApp.");
}
