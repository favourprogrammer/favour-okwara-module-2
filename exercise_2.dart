Map<int, List<String>> winningApps = {
  2012: [
    "FNB, Banking",
    "Discovery HealthId",
    "Matchy",
    "Plascon",
    "Rapid Targets",
    "Transunion dealers guide"
  ],
  2013: [
    "Bookly",
    "Deloitte Digital",
    "DStv",
    "Guardian Buddy",
    "Kids Aid",
    "Markitshare",
    "Nedbank",
    "Price Check",
    "Snapscan"
  ],
  2014: [
    "Live Inspect",
    "My Belongings",
    "Rea Vaya",
    "SuperSport",
    "Sync Mobile",
    "Vigo",
    "Wildlife",
    "Zapper"
  ],
  2015: ["CPUT", "DStv Now", "Eskomsepush", "m4jam", "Vula Mobile", "Wumdrop"],
  2016: [
    "Domestly",
    "Friendly Math Monsters For Kindergarten",
    "Hear ZA",
    "iKhokha",
    "Kaching",
    "MiBrand"
  ],
  2017: [
    "OrderIn",
    "Intergratme",
    "Pick N Pay Super Animals",
    "Transunion 1check",
    "Hey Jude",
    "ORU Social Touch SA",
    "Awethu Project",
    "Zulzi",
    "Shyft",
    "Ecoslips.com"
  ],
  2018: [
    "Cowa Bunga",
    "ACGL",
    "Besmarta",
    "Xander English",
    "Ctrl",
    "Pineapple",
    "Bestee",
    "Stokfella",
    "African Snakebite Institute"
  ],
  2019: [
    "Digger",
    "Si Realities",
    "Vula Mobile",
    "Hydra Farm",
    "Matric Live",
    "Franc",
    "Over",
    "LocTransie",
    "Naked Insurance",
    "My pregnancy Journey",
    "Loot Defense",
    "Mowash"
  ],
  2020: [
    "Checkers Sixty60",
    "Easy Equities",
    "Birdpro",
    "Technishen",
    "Matric Live",
    "Stokfella",
    "Bottles",
    "Lexie Hearing",
    "League Of Legends AR",
    "Examsta",
    "Xitsonga Dictionary",
    "GreenFingers Mobile",
    "Guardian Health",
    "My Pregnant Journey"
  ],
  2021: [
    "Ambani-Afrika",
    "Takealot.com",
    "Shyft",
    "iiDENTIFii",
    "SISA",
    "Guardian Health",
    "Murimi",
    "UniWise",
    "Kazi",
    "Rekindle",
    "Hellopay",
    "Roadsave"
  ],
};

List<String> getWinnersByYear(int year) {
  // get list of winning apps for given year
  List<String>? winners = winningApps[year];

  if (winners != null) {
    // make winners list mutable and then sort winning apps by their name
    winners = List.from(winners);
    winners.sort(((a, b) => a.toLowerCase().compareTo(b.toLowerCase())));
    return winners;
  }

  return [];
}

void main(List<String> args) {
  // Step #2: Store and print winning apps of given years
  List<String> winners2017 = getWinnersByYear(2017);
  List<String> winners2018 = getWinnersByYear(2018);

  // Print the total numbers of apps & app name of the given year
  print("${winners2017.length} apps won in 2017: $winners2017");
  print("${winners2018.length} apps won in 2018: $winners2018");
}
