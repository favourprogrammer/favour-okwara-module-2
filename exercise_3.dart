class App {
  String name = "", category = "", developer = "";
  int winningYear = 0;

  App(this.name, this.category, this.developer, this.winningYear);

  String getUppercaseName() {
    return name.toUpperCase();
  }
}

void main(List<String> args) {
  App ambaniAfrica =
      new App("Ambani Africa", "Education", "Mukundi Lambani", 2021);

  print(ambaniAfrica.getUppercaseName());
}
